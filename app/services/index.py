from app.models.service_info import *
from app.config import settings


def get_service_info():
    return ServiceInfo(version=settings.SERVICE_VERSION, hash=settings.SERVICE_HASH)
