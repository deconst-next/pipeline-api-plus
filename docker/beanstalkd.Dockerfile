FROM alpine
LABEL maintainer="Edward Rodriguez <Edward.Rodriguez@rackspace.com>"

RUN apk add --no-cache beanstalkd

COPY ./docker/start.sh /start.sh

RUN mkdir /databeans \
    && chmod +x /start.sh \
    && adduser -D -u 1000 1001 \
    && chown -R 1001:1001 /databeans \
    && chmod -R 775 /databeans

VOLUME ["/databeans"]

USER 1001

EXPOSE 11300
CMD ["/usr/bin/beanstalkd"]
