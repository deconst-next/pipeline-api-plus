# FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7
FROM python:3.7

LABEL maintainer="Edward Rodriguez <Edward.Rodriguez@rackspace.com>"

ENV APP_MODULE "app.main:api"

COPY ./app /app/app/
COPY ./Pipfile* /app/

# RUN pip install requests pytest tenacity passlib[bcrypt] couchbase "fastapi>=0.16.0"

# Remove image prepackaged filed
# RUN rm ./main.py ./prestart.sh && \
# RUN pip install pipenv && \
#     pipenv install --system --dev
RUN pip install "fastapi==0.33.0" "pydantic==0.30" greenstalk requests pytest email-validator

# CMD [ "/usr/local/bin/pytest -v" ]
# This will make the container wait, doing nothing, but alive
CMD ["bash", "-c", "while true; do sleep 1; done"]
